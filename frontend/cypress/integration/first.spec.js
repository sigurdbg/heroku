describe('Create exercise', () => {
    it('Exercise creation', function() {
        this.login(this.athleteUser)
        cy.get('#nav-exercises').click()
        cy.get('#btn-create-exercise').click()
        cy.url().should('include', 'exercise.html')
//
        typeIn(
            'Workout',
            'Description',
            '10',
            '-10',
            '-10',
            'Legs',
        )
        cy.intercept({method: 'POST', url: '/api/exercises/' }).as('createRequest')
        cy.get('#btn-ok-exercise').click()
        cy.wait('@createRequest')
        // Successfully created exercise
        cy.url().should('include', 'exercises.html')
    })


    function typeIn(name, description, unit, duration, calories, muscleGroup) {
        cy.get('#inputName', { timeout: 10000 }).type(name)
        cy.get('#inputDescription').type(description)
        cy.get('#inputUnit').type(unit)
        cy.get('#inputDuration').type(duration)
        cy.get('#inputCalories').type(calories)
        cy.get('select[name="muscleGroup"]').eq(0).select(muscleGroup, {force: true})
    }

})
