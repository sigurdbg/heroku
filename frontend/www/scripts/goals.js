async function fetchExerciseTypes(request) {
    let response = await sendRequest("GET", `${HOST}/api/goal/`);

    if (response.ok) {
        let data = await response.json();

        let goals = data.results;
        let container = document.getElementById('div-content');
        let exerciseTemplate = document.querySelector("#template-exercise");
        goals.forEach(goal => {
            const exerciseAnchor = exerciseTemplate.content.firstElementChild.cloneNode(true);
            exerciseAnchor.href = `goal.html?id=${goal.id}`;

            const h2 = exerciseAnchor.querySelector("h2");
            h2.textContent = goal.name;

            const h6 = exerciseAnchor.querySelector("h6");
            h6.textContent = goal.date;

            const h5 = exerciseAnchor.querySelector("h5");
            h5.textContent += goal.exercise;

            const p = exerciseAnchor.querySelector("p");
            p.textContent = goal.description;

            const a = exerciseAnchor.querySelector("a");
            a.href = `goal.html?id=${goal.id}`;

            container.appendChild(exerciseAnchor);
        });
    }

    return response;
}

function createExercise() {
    window.location.replace("goal.html");
}

async function init() {
    let response = await sendRequest("GET", `${HOST}/api/goal/`);

    if (response.ok) {
        let data = await response.json();
        data.results.map((goal) => {console.log(goal)});
    }else{
        console.log("ERROR")
    }

    return response;
}


window.addEventListener("DOMContentLoaded", async () => {
    let createButton = document.querySelector("#btn-create-goal");
    createButton.addEventListener("click", createExercise);

    let response = await fetchExerciseTypes();
    await init();

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve exercise types!", data);
        document.body.prepend(alert);
    }
});
