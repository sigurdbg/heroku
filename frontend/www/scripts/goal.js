let cancelButton;
let okButton;
let deleteButton;
let editButton;
let oldFormData;


function handleCancelButtonDuringCreate() {
    window.location.replace("goals.html");
}

async function createGoal() {

    let form = document.querySelector("#form-goal");
    let formData = new FormData(form);
    let body = {"name": formData.get("name"),
                "description": formData.get("description"),
                "date": formData.get("date"),
                "exercise": formData.get("exercise")};

      let response = await sendRequest("POST", `${HOST}/api/goal/`, body);

      if (response.ok) {
          window.location.replace("goals.html");
      } else {
          let data = await response.json();
          let alert = createAlert("Could not create new goal!", data);
          document.body.prepend(alert);
      }

}


async function deleteExercise(id) {
    let response = await sendRequest("DELETE", `${HOST}/api/goal/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not delete goal ${id}`, data);
        document.body.prepend(alert);
    } else {
        window.location.replace("goals.html");
    }
}

async function retrieveGoal(id) {
    let response = await sendRequest("GET", `${HOST}/api/goal/${id}/`);

    console.log(response)

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve goal data!", data);
        document.body.prepend(alert);
    } else {
        let goalData = await response.json();
        let form = document.querySelector("#form-goal");
        let formData = new FormData(form);

        for (let key of formData.keys()) {
            let selector
            key !== "exercise" ? selector = `input[name="${key}"], textarea[name="${key}"]` : selector = `select[name=${key}]`
            let input = form.querySelector(selector);
            let newVal = goalData[key];
            if (key === "date") {
                // Creating a valid datetime-local string with the correct local time
                let date = new Date(newVal);
                date = new Date(date.getTime() - (date.getTimezoneOffset() * 60 * 1000)).toISOString(); // get ISO format for local time
                newVal = date.substring(0, newVal.length - 1);    // remove Z (since this is a local time, not UTC)
            }

            input.value = newVal;

        }
    }
}

async function updateExercise(id) {
    let form = document.querySelector("#form-goal");
    let formData = new FormData(form);

    let muscleGroupSelector = document.querySelector("select")
    muscleGroupSelector.removeAttribute("disabled")


    let body = {"name": formData.get("name"),
        "description": formData.get("description"),
        "date": formData.get("date"),
        "exercise": formData.get("exercise")};

    let response = await sendRequest("PUT", `${HOST}/api/goal/${id}/`, body);

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not update goal ${id}`, data);
        document.body.prepend(alert);
    } else {
        console.log(response)
        window.location.replace("goals.html");

    }
}

const init = async () => {
    let goalContainer = document.getElementById('exercise-content');
    await sendRequest("GET", `${HOST}/api/exercises/`).then(async (val) => {
        val.json().then((resp) => {
            resp.results.forEach(workout => {
                if(workout){
                    newOption = document.createElement('option');
                    newOption.value = workout.name;

                    if (typeof newOption.textContent === 'undefined')
                    {
                        newOption.innerText = workout.name;
                    }
                    else
                    {
                        newOption.textContent = workout.name;
                    }

                    goalContainer.appendChild(newOption);
                }
            });
        })
    })

}

window.addEventListener("DOMContentLoaded", async () => {
    await init();

    cancelButton = document.querySelector("#btn-cancel-goal");
    okButton = document.querySelector("#btn-ok-goal");
    deleteButton = document.querySelector("#btn-delete-goal");
    oldFormData = null;

    const urlParams = new URLSearchParams(window.location.search);

    // view/edit
    if (urlParams.has('id')) {
        const goalID = urlParams.get('id');
        await retrieveGoal(goalID);

        deleteButton.addEventListener("click", (async (id) => await deleteExercise(id)).bind(undefined, goalID));
        okButton.addEventListener("click", (async (id) => await updateExercise(id)).bind(undefined, goalID));
        cancelButton.addEventListener("click", handleCancelButtonDuringCreate);

    }else{
        cancelButton.className = cancelButton.className.replace(" hide", "");
        cancelButton.className = cancelButton.className += "hide";
        deleteButton.className = deleteButton.className += "hide";

        okButton.addEventListener("click", async () => await createGoal());
    }

});
